#: tasks.py:90
msgid   "License"
msgstr  ""

#: tasks.py:91
msgid   "Version"
msgstr  ""

#: tasks.py:92 bugs.py:114
msgid   "Summary"
msgstr  ""

#: tasks.py:93 bugs.py:123
msgid   "Last update:"
msgstr  ""

#: tasks.py:94 tasks.py:150
msgid   "Debian package not available"
msgstr  ""

#: tasks.py:96
msgid   "For a better overview of the project's availability as a Debian package, each head row "
        "has a color code according to this scheme:"
msgstr  ""

#: tasks.py:97
#, python-format
msgid   "If you discover a project which looks like a good candidate for %s\n"
        "                              to you, or if you have prepared an unofficial Debian "
        "package, please do not hesitate to\n"
        "                              send a description of that project to the <a href=\"mailto:"
        "%s\">%s mailing list</a>"
msgstr  ""

#: tasks.py:101
#, python-format
msgid   "The list to the right includes various software projects which are of some interest to "
        "the %s Project. Currently, only a few of them are available as Debian packages. It is our "
        "goal, however, to include all software in %s which can sensibly add to a high quality "
        "Debian Pure Blend."
msgstr  ""

#: tasks.py:102
msgid   "Tasks page"
msgstr  ""

#: tasks.py:103
msgid   "Project"
msgstr  ""

#: tasks.py:104
#, python-format
msgid   "This is a list of the Tasks %s is made of:"
msgstr  ""

#: tasks.py:105
msgid   "This page is also available in the following languages:"
msgstr  ""

#: tasks.py:106
#, python-format
msgid   "How to set <a href=\"%s\">the default document language</a>"
msgstr  ""

#: tasks.py:108 bugs.py:262
msgid   "Homepage not available"
msgstr  ""

#: tasks.py:109
msgid   "Translate description"
msgstr  ""

#: tasks.py:110
msgid   "Fix translated description"
msgstr  ""

#: tasks.py:111
msgid   "Popularitycontest results: number of people who use this package regularly (number of "
        "people who upgraded this package recently) out of"
msgstr  ""

#: tasks.py:112
msgid   "Table of contents"
msgstr  ""

#: tasks.py:113
msgid   "complete packagelist"
msgstr  ""

#: tasks.py:125
msgid   "Packages"
msgstr  ""

#: tasks.py:126
#, python-format
msgid   "A %sDebian Pure Blend%s is a Debian internal project which assembles\n"
        "a set of packages that might help users to solve certain tasks of their work.  The list "
        "on\n"
        "the right shows the tasks of %s."
msgstr  ""

#: tasks.py:133
msgid   "Official Debian packages with high relevance"
msgstr  ""

#: tasks.py:136
msgid   "Official Debian package"
msgstr  ""

#: tasks.py:137
msgid   "Official Debian packages with lower relevance"
msgstr  ""

#: tasks.py:139
msgid   "Debian packages in contrib or non-free"
msgstr  ""

#: tasks.py:140
msgid   "Debian package in contrib/non-free"
msgstr  ""

#: tasks.py:141
msgid   "Debian packages in experimental"
msgstr  ""

#: tasks.py:142
msgid   "Debian package in experimental"
msgstr  ""

#: tasks.py:143
msgid   "Debian packages in New queue (hopefully available soon)"
msgstr  ""

#: tasks.py:144
msgid   "New Debian package"
msgstr  ""

#: tasks.py:145
msgid   "Packaging has started and developers might try the packaging code in VCS"
msgstr  ""

#: tasks.py:146
msgid   "Unofficial Debian package"
msgstr  ""

#: tasks.py:147
msgid   "Unofficial packages built by somebody else"
msgstr  ""

#: tasks.py:149
msgid   "No known packages available"
msgstr  ""

#: tasks.py:151
msgid   "No known packages available but some record of interest (WNPP bug)"
msgstr  ""

#: tasks.py:200
msgid   "Maintainer"
msgstr  ""

#: tasks.py:202
msgid   "Responsible"
msgstr  ""

#: tasks.py:224 bugs.py:106
msgid   "Links to other tasks"
msgstr  ""

#: tasks.py:225
msgid   "Index of all tasks"
msgstr  ""

#: bugs.py:107
msgid   "Tasks"
msgstr  ""

#: bugs.py:108
msgid   "Tasks overview"
msgstr  ""

#: bugs.py:109
msgid   "Legend"
msgstr  ""

#: bugs.py:110
msgid   "Bugs of package"
msgstr  ""

#: bugs.py:111
msgid   "Total bugs"
msgstr  ""

#: bugs.py:112
msgid   "Open bugs"
msgstr  ""

#: bugs.py:113
msgid   "Fixed bugs"
msgstr  ""

#: bugs.py:115
#, python-format
msgid   "A %sDebian Pure Blend%s is a Debian internal project which assembles\n"
        "a set of packages that might help users to solve certain tasks of their work.  This page "
        "should be helpful\n"
        "to track down the bugs of packages that are interesting for the %s project to enable "
        "developers a quick\n"
        "overview about possible problems."
msgstr  ""

#: bugs.py:121
msgid   "Bugs page"
msgstr  ""

#: bugs.py:122
msgid   "This is a list of metapackages.  The links are leading to the respective bugs page."
msgstr  ""

#: bugs.py:124
msgid   "To estimate the overall status of the packages in the dependencies of\n"
        "a metapackage a weighted severity is calculated.  Done bugs are ignored and bugs in "
        "dependent and\n"
        "recommended packages are weighted by factor three compared to suggested packages.  "
        "Release critical\n"
        "bugs have a much larger weight than important, while the contribution of normal bugs is "
        "even smaller\n"
        "and minor bugs have a very small weight.  Wishlist bugs are ignored in this calculation.  "
        "The resulting\n"
        "sum is compared to some boundaries to find a verbal form.  The actual numbers need some "
        "adjustment\n"
        "to make real sense - this evaluation method is in testing phase."
msgstr  ""

#: bugs.py:131
msgid   "The severities of bugs are weighted as follows"
msgstr  ""

#: bugs.py:222
msgid   "Metapackage is in excellent shape"
msgstr  ""

#: bugs.py:225
msgid   "Metapackage is in very good shape"
msgstr  ""

#: bugs.py:228
msgid   "Metapackage is in good shape"
msgstr  ""

#: bugs.py:231
msgid   "Consider looking into bugs of this metapackage"
msgstr  ""

#: bugs.py:234
msgid   "Looking into bugs of this metapackage is recommended"
msgstr  ""

#: bugs.py:237
msgid   "Immediately looking into bugs of the dependencies of this metapackage is advised"
msgstr  ""

#: bugs.py:242
msgid   "Open bugs in dependent packages"
msgstr  ""

#: bugs.py:243
msgid   "Open bugs in suggested packages"
msgstr  ""

#: bugs.py:244
msgid   "Done bugs"
msgstr  ""

#: bugs.py:246
msgid   "No open bugs in dependent packages"
msgstr  ""

#: bugs.py:247
msgid   "No open bugs in suggested packages"
msgstr  ""

#: bugs.py:248
msgid   "No done bugs"
msgstr  ""

#: bugs.py:263
msgid   "Not maintained in Vcs"
msgstr  ""

#: bugs.py:264
msgid   "Vcs"
msgstr  ""

#: webconf/debian-med.conf:13
msgid   "Help us to see Debian used by medical practitioners and biomedical researchers! Join us "
        "on the <a href=\"https://salsa.debian.org/med-team/\">Salsa project page</a>."
msgstr  ""

#
# These strings occured in the old *.php files and the translations should be
# preserved for later use.
# Just keep this file
#
#: ../bug_details.tmpl:8 ../bugs.tmpl:7 ../ddtp.tmpl:7 ../ddtp_details.tmpl:8 ../locales.php:7
#: ../tasks.tmpl:8 ../tasks_idx.tmpl:8
msgid   "summary"
msgstr  ""

#: ../bug_details.tmpl:12 ../bugs.tmpl:11
msgid   "Bugs for package"
msgstr  ""

#: ../bug_details.tmpl:40 ../bugs.tmpl:66 ../ddtp.tmpl:30 ../ddtp_details.tmpl:34 ../tasks.tmpl:112
#: ../tasks_idx.tmpl:29
msgid   "Last update"
msgstr  ""

#: ../bug_details.tmpl:41 ../bugs.tmpl:67 ../ddtp.tmpl:31 ../ddtp_details.tmpl:35 ../tasks.tmpl:113
#: ../tasks_idx.tmpl:30
msgid   "Please note: this page gets automatically updated twice a day, on 00:00 and 12:00 UTC."
msgstr  ""

#: ../bugs.tmpl:19 ../bugs.tmpl:31
msgid   "Summary bugs page"
msgstr  ""

#: ../ddtp.tmpl:15
msgid   "DDTP Statistics"
msgstr  ""

#: ../ddtp_details.tmpl:12
msgid   "Summary for package"
msgstr  ""

#: ../inc/header.inc.php:34
#, php-format
msgid   "Help us to see Debian used by medical practicioners and researchers! Join us on the "
        "%sSalsa page%s."
msgstr  ""

#: ../index.php:7
msgid   "information"
msgstr  ""

#: ../index.php:11
msgid   "Developers please visit our"
msgstr  ""

#: ../index.php:12
msgid   "Wiki page"
msgstr  ""

#: ../index.php:15
msgid   "The Debian Med project presents packages that are associated with <ul><li>medicine</"
        "li><li>pre-clinical research</li><li>life science.</li></ul> Its developments are mostly "
        "focused on three areas for the moment: <ul><li>medical practice</li><li>imaging</"
        "li><li>bioinformatics</li></ul>and can be installed directly from every Debian "
        "installation."
msgstr  ""

#: ../index.php:22
msgid   "warning"
msgstr  ""

#: ../index.php:24 ../locales.php:26
#, php-format
msgid   "Your browser uses language settings that we could not yet provide translations for.<br /"
        ">If you can spare one to two hours then please consider to help us in translating our "
        "pages for your people, too. Instructions are found %shere%s."
msgstr  ""

#: ../index.php:29
#, php-format
msgid   "Visit the %sLocalization page%s."
msgstr  ""

#: ../index.php:37
msgid   "pages"
msgstr  ""

#: ../index.php:42
msgid   "Group policy"
msgstr  ""

#: ../index.php:43
msgid   "Bugs"
msgstr  ""

#: ../index.php:44
msgid   "Quality Assurance"
msgstr  ""

#: ../index.php:45
msgid   "Debian Description Translation Project"
msgstr  ""

#: ../index.php:46
msgid   "Tasks of our Blend"
msgstr  ""

#: ../index.php:47
msgid   "SVN repository"
msgstr  ""

#: ../index.php:49
msgid   "Localizations"
msgstr  ""

#: ../index.php:54
msgid   "members"
msgstr  ""

#: ../index.php:72 ../index.php:92
msgid   "Project Administrator"
msgstr  ""

#: ../index.php:77 ../index.php:96
msgid   "Project Developer"
msgstr  ""

#: ../index.php:91
msgid   "Green Wheel"
msgstr  ""

#: ../index.php:95
msgid   "Grey Wheel"
msgstr  ""

#: ../index.php:101
msgid   "UTC time"
msgstr  ""

#: ../index.php:107
msgid   "badges"
msgstr  ""

#: ../index.php:113
msgid   "Valid XHTML 1.1"
msgstr  ""

#: ../index.php:118
msgid   "Valid CSS 2"
msgstr  ""

#: ../index.php:125
msgid   "Berkeley Open Infrastructure for Network Computing"
msgstr  ""

#: ../index.php:132
msgid   "recent activity"
msgstr  ""

#: ../index.php:138
msgid   "date"
msgstr  ""

#: ../index.php:139 ../locales.php:50
msgid   "author"
msgstr  ""

#: ../index.php:140
msgid   "content"
msgstr  ""

#: ../index.php:141
msgid   "link"
msgstr  ""

#: ../index.php:170
msgid   "todo"
msgstr  ""

#: ../index.php:220
msgid   "Please, note that this is a SVN export of our website. It might break during SVN commits."
msgstr  ""

#: ../locales.php:11
msgid   "Current locale"
msgstr  ""

#: ../locales.php:15
msgid   "Priority"
msgstr  ""

#: ../locales.php:31
#, php-format
msgid   "More information on how to contribute to the Debian Med project, can be found in the "
        "%sHow to Contribute%s page."
msgstr  ""

#: ../locales.php:41
msgid   "localization"
msgstr  ""

#: ../locales.php:45
msgid   "Currently installed locales"
msgstr  ""

#: ../locales.php:48
msgid   "locale"
msgstr  ""

#: ../locales.php:49
msgid   "translation status"
msgstr  ""

#: ../locales.php:51
msgid   "team"
msgstr  ""

#: ../locales.php:92
msgid   "Add new locale"
msgstr  ""

#: ../tasks.tmpl:18
msgid   "The list to the right includes various software projects which are of some interest to "
        "the Debian Med Project."
msgstr  ""

#: ../tasks.tmpl:19
msgid   "Currently, only a few of them are available as Debian packages."
msgstr  ""

#: ../tasks.tmpl:20
msgid   "It is our goal, however, to include all software in Debian Med which can sensibly add to "
        "a high quality Debian Pure Blend."
msgstr  ""

#: ../tasks.tmpl:31
#, php-format
msgid   "If you discover a project which looks like a good candidate for Debian Med to you, or if "
        "you have prepared an inofficial Debian package, please do not hesitate to send a "
        "description of that project to the %sDebian Med mailing list%s"
msgstr  ""

#
# These strings are manually added, they
# come from inside the Python scripts on
# Salsa.
#
# Please keep the static.pot file.
#
#
# update-bugs
#
msgid   "Subject"
msgstr  ""

msgid   "Sender"
msgstr  ""

msgid   "Tags"
msgstr  ""

msgid   "Date"
msgstr  ""

msgid   "Severity"
msgstr  ""

msgid   "Found in"
msgstr  ""

msgid   "Fixed in"
msgstr  ""

#
# update-ddtp
#
msgid   "translation not available"
msgstr  ""

msgid   "yes"
msgstr  ""

msgid   "translated"
msgstr  ""

msgid   "edit"
msgstr  ""

msgid   "edit translation"
msgstr  ""

msgid   "untranslated"
msgstr  ""

msgid   "Please follow the link below to start translating"
msgstr  ""
