'''To enable better formatting of long descriptions we use Markdown
and some preprocessing of the RFC822 formatted descriptions
Copyright 2008: Andreas Tille <tille@debian.org>
License: GPL
'''

import re
from sys import stderr
from markdown import markdown            # Markdown
from docutils.core import publish_parts  # alternatively reST
from genshi import Markup                # Mark the string as rendered

detect_list_start_re = re.compile("^\s+[-*+]\s+")
detect_code_start_re = re.compile("^\s")
detect_code_end_re = re.compile("^[^\s]")
detect_url_re = re.compile("[fh]t?tp://")


def PrepareMarkdownInput(lines):
    lines = lines.splitlines()
    ret = ''
    inlist = 0
    incode = 0
    for line in lines:
        # strip leading space from description as well as useless trailing
        line = re.sub('^ ', '', line.rstrip())

        # a '^\.$' marks in descriptions a new paragraph,
        # markdown uses an empty line here
        line = re.sub('^\.$', '', line)

        # In long descriptions 'o' and '.' are quite frequently used
        # as bullet in lists which is not recognised by markdown.
        # So just turn '[.o]' into '*' to let markdown do
        # its work successfully.
        line = re.sub('^(\s*)[.o]\s+', '\\1* ', line)

        # To enable Markdown debugging and verbose output in remarks
        # a 'o'/'.' is inserted as '\o'/'\.' in remarks
        # - the original is restored here:
        line = re.sub('^(\s*)\\\\([.o]\s+)', '\\1\\2', line)

        if detect_code_start_re.search(line):
            # If a list or verbatim mode starts MarkDown needs an empty line
            if incode == 0:
                ret += "\n"
                incode = 1
                if detect_list_start_re.search(line):
                    inlist = 1
        if incode == 1 and inlist == 0:
            # Add a leading tab if in verbatim but not in list mode
            ret += "\t"
        # If there is an empty line or a not indented line the list
        # or verbatim text ends. It is important to check for empty lines
        # because some descriptions would insert more lines than needed
        # in verbose mode (see for instance glam2)
        if (detect_code_end_re.search(line) or line == '') and incode == 1:
            inlist = 0  # list ends if indentation stops
            incode = 0  # verbatim mode ends if indentation stops
        # Mask # at first character in line which would lead to
        #   MARKDOWN-CRITICAL: "We've got a problem header!"
        # otherwise
        if line.startswith('#'):
            ret += '\\'
# The following is already handled by markdown itself, but confuses the
# use of Markdown in the description, like for
# line = '... [Scisoft](http://www.eso.org/scisoft)'
#        if detect_url_re.search(line):
#            # some descriptions put URLs in '<>' which is unneeded and might
#            # confuse the parsing of '&' in URLs which is needed sometimes
#            line = re.sub('<*([fh]t?tp://[-./\w?=~;&%]+)>*',
#                          '[\\1](\\1)', line)
        ret += line + "\n"
    return ret

def render_longdesc(MarkDownInput):
    global rendering_lib
    if rendering_lib == 'rest':
        try:
            LongDesc = publish_parts(MarkDownInput, writer_name='html')['body']
        except:
            stderr.write("Unable to render the following prepared text:\n%s\n"
                         % MarkDownInput)
            LongDesc = "Problems in rendering description using reST"
    else:  # by default use Markdown
        LongDesc = markdown(MarkDownInput)
    return Markup(LongDesc)


def SplitDescription(description):
    # Split first line of Description value as short description

    lines = description.splitlines()

    ShortDesc = lines[0]
    LongDesc = PrepareMarkdownInput('\n'.join(lines[1:]))

    return (ShortDesc, LongDesc)

rendering_lib = ''
