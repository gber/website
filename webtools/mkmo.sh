#!/bin/sh -e

domain="blends-webtools"
podir="po"
[ "--force" != "$1" ] || rm -rf "locale"
ls -1 "$podir"/*.po | xargs -I{} basename '{}' .po | while read lang; do
	modir="locale/$lang/LC_MESSAGES"
	mkdir -p "$modir"
	msgfmt --output-file="$modir/$domain.mo" "$podir/$lang.po"
done
