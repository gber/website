#!/usr/bin/python3
# Copyright 2013: Andreas Tille <tille@debian.org>
# License: GPL

import gettext
import json
import os
import re
import sys
import time
from datetime import date, datetime
from email.utils import formatdate

from genshi import Markup
from genshi.template import TemplateLoader

from blendstasktools import (CheckOrCreateOutputDir, ReadConfig,
                             RowDictionaries, SetFilePermissions,
                             _execute_udd_query, curs)

SLOWQUERYREPORTLIMIT = 30

debug = 1

queries = [
    """
CREATE TEMPORARY VIEW autopkgtest_fail (source, suite_arch, messages) AS
  SELECT
    source,
    suite_arch,
    messages
  FROM (
    SELECT
      source,
      string_agg(suite_arch, ',') AS suite_arch,
      array_agg(message) AS messages
    FROM (
      SELECT
        source,
        string_agg(suite || '_' || arch, ',') AS suite_arch,
        message
      FROM ci
      WHERE status = 'fail'
      GROUP BY
        source,
        message
     ) tmp1 GROUP BY source
  ) tmp2
  WHERE (
    -- Make sure not only neutral test in stable will raise a signal here
    suite_arch LIKE '%testing%'
    OR suite_arch LIKE '%unstable%'
  );

CREATE TEMPORARY VIEW autopkgtest_neutral (source, suite_arch, messages) AS
  SELECT
    source,
    suite_arch,
    messages
  FROM (
    SELECT
      source,
      string_agg(suite_arch, ',') AS suite_arch,
      array_agg(message) AS messages
    FROM (
      SELECT
        source,
        string_agg(suite || '_' || arch, ',') AS suite_arch,
        message
      FROM ci
      WHERE status = 'neutral'
      GROUP BY
        source,
        message
    ) tmp1
    GROUP BY source
  ) tmp2
  WHERE (
    -- Make sure not only neutral test in stable will raise a signal here
    suite_arch LIKE '%testing%'
    OR suite_arch LIKE '%unstable%'
  );
""",
    """
PREPARE query_qa_packages (text) AS
SELECT DISTINCT
  p.source,
  t.tasks,
  ci.status_agg,
  ts.testsuite AS testsuite,
  tags.gui as gui,
  failci.suite_arch AS failed_suites_arch,
  failci.messages AS failed_messages,
  neutralci.suite_arch AS neutral_suites_arch,
  neutralci.messages AS neutral_messages,
  ar.version AS autoremoval_version,
  ar.bugs,
  CAST (to_timestamp(ar.first_seen::numeric) AS date) AS first_seen,
  CAST (to_timestamp(ar.last_checked::numeric) AS date) AS last_checked,
  CAST (to_timestamp(ar.removal_time::numeric) AS date) AS removal,
  rdeps AS autoremoval_rdeps,
  buggy_deps,
  bugs_deps,
  rdeps_popcon,
  m.interesting AS migration_interesting,
  m.in_testing AS migration_in_testing,
  m.in_testing_age AS migration_in_testing_age,
  m.first_seen AS migration_first_seen,
  m.first_seen_age AS migration_first_seen_age,
  m.sync AS migration_sync,
  m.sync_age AS migration_sync_age,
  -- excuses
  me.migration_policy_verdict,
  me.old_version AS excuses_old_version,
  me.new_version AS excuses_new_version,
  me.is_candidate AS excuses_is_candidate,
  me.reason,
  me.dependencies AS excuses_dependencies,
  me.invalidated_by_other_package,
  -- piuparts
  piu.piuparts_fail,
  piu.version AS piuparts_version,
  (
    CAST((removal_time IS NOT NULL) AS INTEGER) * 101 +
    CAST((m.interesting IS NOT NULL) AS INTEGER) * 11 +
    CAST((
      me.item_name != ''
      AND me.migration_policy_verdict IS NOT NULL
      AND me.migration_policy_verdict NOT IN (
        'PASS',
        'REJECTED_TEMPORARILY'
      )
      AND p.release = 'sid'
    ) AS INTEGER) * 21 +
    CAST((piu.piuparts_fail IS NOT NULL) AS INTEGER) * 11 +
    CAST((ci.status_agg IS NOT NULL) AS INTEGER) * 31 +
    CAST((
      ts.testsuite IS NULL
      AND tags.gui IS NULL
    ) AS INTEGER) * 6
  ) AS score
FROM packages p
-- begin tests
LEFT OUTER JOIN (
  SELECT
    source,
    array_agg(status) AS status_agg
  FROM (
    SELECT DISTINCT
      source,
      status
    FROM (
      SELECT
        ci.source,
        ci.suite,
        ci.arch,
        ci.status,
        ci.version,
        regexp_replace(s.version, E'\\\\+b[0-9]+$', '') AS rel_version
      FROM ci ci
      LEFT JOIN (
        SELECT
          source,
          CASE
            WHEN release='sid' THEN 'unstable'
            WHEN release='bullseye' THEN 'testing'
            ELSE release
          END AS suite,
          architecture AS arch,
          version
        FROM packages
      ) s ON (
        ci.source = s.source
        AND ci.suite = s.suite
        AND (ci.arch=s.arch OR s.arch='all')
      )
    ) tmp
    WHERE (
      (
        version >= rel_version
        OR rel_version IS NULL
      )
      AND status != 'pass'
      AND suite IN ('testing', 'unstable')
      AND version != 'n/a'
    )
  ) tmp
  GROUP BY source
) ci ON p.source = ci.source
LEFT OUTER JOIN autopkgtest_fail failci ON p.source = failci.source
LEFT OUTER JOIN autopkgtest_neutral neutralci ON
  p.source = neutralci.source
LEFT OUTER JOIN (
  SELECT
    source,
    testsuite
  FROM (
    SELECT DISTINCT
      source,
      testsuite,
      row_number() OVER (
        PARTITION BY source
        ORDER BY version DESC
      )
    FROM sources
    WHERE release='sid'
  ) tmp
  WHERE row_number = 1
) ts ON p.source = ts.source
LEFT OUTER JOIN (
  SELECT
    source,
    gui
  FROM (
    SELECT
      source,
      gui,
      row_number() OVER (PARTITION BY source ORDER BY gui DESC)
    FROM (
      SELECT DISTINCT
        p.package,
        p.source,
        gui
      FROM packages p
      LEFT JOIN (
        SELECT
          package,
          'gui' AS gui
        FROM debtags
        WHERE
          tag LIKE 'uitoolkit::%' OR
          tag LIKE '%x11%'
        GROUP BY package
      ) t ON p.package = t.package
      WHERE release ='sid'
    ) dt
  ) st
  WHERE row_number = 1
) tags ON p.source = tags.source
-- end tests
LEFT OUTER JOIN testing_autoremovals ar ON p.source = ar.source
LEFT OUTER JOIN (
  SELECT
    source,
    True AS interesting,
    in_testing,  -- Date package was last seen in testing
    current_date - in_testing AS in_testing_age,
    first_seen,  -- Date package was first seen in Debian
    current_date - first_seen AS first_seen_age,
    sync,  -- Date package was same version in testing and unstable
    current_date - sync AS sync_age
  FROM migrations
  WHERE
    current_date - in_unstable < 2  -- Not removed from unstable
    AND (sync IS NULL OR current_date - sync > 11) -- Not migrated recently
    AND source NOT IN (
      SELECT
        source
      FROM upload_history
      WHERE
        date > (current_date - interval '10 days')
        AND distribution='unstable'
    ) -- Not uploaded to unstable in the last 10 days
    AND current_date - first_seen > 11  -- Not recently uploaded
    AND (
      in_testing IS NULL
      OR current_date - in_testing > 1
      OR sync IS NULL
      OR current_date - sync > 11
    )  -- Testing/unstable not synced recently while in testing
) m ON p.source = m.source
LEFT OUTER JOIN migration_excuses me ON p.source = me.source
LEFT OUTER JOIN (
  SELECT
    source,
    piuparts_fail,
    version
  FROM (
    SELECT
      array_agg(section) AS piuparts_fail,
      source,
      version,
      row_number() OVER (PARTITION BY source ORDER BY version DESC)
    FROM piuparts_status
    WHERE status = 'fail'
    GROUP BY
      version,
      source
  ) tmp1
  WHERE row_number = 1
) piu ON p.source = piu.source
LEFT OUTER JOIN (
  SELECT
    source,
    array_agg(task) AS tasks
  FROM (
    SELECT DISTINCT
      ps.source,
      bd.task
    FROM blends_dependencies bd
    INNER JOIN (
      SELECT DISTINCT
        package,
        source
      FROM packages
      WHERE release = 'sid'
    ) ps ON bd.package = ps.package
    WHERE blend = $1
  ) tmp
  GROUP BY source
) t ON p.source = t.source
WHERE
  p.package IN (
    SELECT package
    FROM blends_dependencies
    WHERE blend = $1
  )
  AND p.release = 'sid'
ORDER BY score DESC
""",
    # What tasks are involved
    """
PREPARE query_get_tasks (text) AS
SELECT
  task,
  title,
  description,
  long_description
FROM blends_tasks
WHERE blend = $1
ORDER BY task;
""",
]


def _get_source_directory(source):
    """Return the directory path fragment in URLs for a source package.

    First four chars if sources starts with lib else first char.

    """
    if source.startswith('lib'):
        return source[0:4]

    return source[0]


def _get_tasks(blend_name):
    """Return a list of tasks by querying UDD."""
    _execute_udd_query("EXECUTE query_get_tasks('%s')" % blend_name)
    if curs.rowcount <= 0:
        sys.stderr.write("No tasks metadata received for Blend %s\n" %
                         blend_name)
        sys.exit(1)

    data = {}
    for row in RowDictionaries(curs):
        data[row['task']] = {
            'title': row['title'],
            'description': row['description'],
            'long_description': row['long_description'],
            'autoremovals': 0,
            'migrations': 0,
            'autopkgtests': 0,
            'piuparts': 0,
            'no_issues': 0,
            'total': 0,
            'data': []
        }

    return data


def _get_ci_data_from_row(qa_data, row):
    """Return Autopkgtests (CI) data from a single database row."""
    ci_data = {}
    ci_data['status_agg'] = row['status_agg']
    ci_data['testsuite'] = row['testsuite']
    ci_data['gui'] = row['gui']

    # Highest priority status wins: fail > tmpfail > neutral > pass
    ci_data['status'] = None
    if row['status_agg']:
        for status in ('fail', 'tmpfail', 'neutral', 'pass'):
            if status in row['status_agg']:
                ci_data['status'] = status
                break

    # Packages with 'pass' status don't have any CI data.
    if ci_data['status']:
        for task in row['tasks']:
            qa_data[task]['autopkgtests'] += 1

    ci_data['neutral_suites_arch'] = []
    if row['neutral_suites_arch']:
        for suite_arch in row['neutral_suites_arch'].split(","):
            (suite, arch) = suite_arch.split("_")
            ci_data['neutral_suites_arch'].append({
                'suite': suite,
                'arch': arch
            })
    ci_data['neutral_messages'] = row['neutral_messages'] or []

    ci_data['failed_suites_arch'] = []
    if row['failed_suites_arch']:
        for suite_arch in row['failed_suites_arch'].split(","):
            (suite, arch) = suite_arch.split("_")
            ci_data['failed_suites_arch'].append({
                'suite': suite,
                'arch': arch
            })
    ci_data['failed_messages'] = row['failed_messages'] or []

    return ci_data


def _process_row_data(row, data, keys):
    """Process database row data into dict for given keys."""
    for key in keys:
        if isinstance(row[key], date):
            # Avoid "Object of type date is not JSON serializable"
            data[key] = str(row[key])
        else:
            data[key] = row[key]


def _get_ar_data_from_row(qa_data, row):
    """Return Auto-removals data from a single database row."""
    data = {}
    keys = ('autoremoval_version', 'first_seen', 'last_checked', 'removal',
            'rdeps_popcon')
    _process_row_data(row, data, keys)

    for key in ('bugs', 'autoremoval_rdeps', 'buggy_deps', 'bugs_deps'):
        value = row[key]
        data[key] = value.split(',') if value else []

    if row['autoremoval_version']:
        for task in row['tasks']:
            qa_data[task]['autoremovals'] += 1

    return data


def _get_excuses_data_from_row(qa_data, row):
    """Return excuses data from a single database row."""
    data = {}
    keys = (
        'migration_interesting',
        'migration_in_testing',
        'migration_in_testing_age',
        'migration_first_seen',
        'migration_first_seen_age',
        'migration_sync',
        'migration_sync_age',
        'migration_policy_verdict',
        'excuses_old_version',
        'excuses_new_version',
        'excuses_is_candidate',  # boolean
        'reason',  # array
        'excuses_dependencies',  # JSON string
        'invalidated_by_other_package'  # boolean
    )
    _process_row_data(row, data, keys)

    if not data['migration_interesting']:
        data['migration_result'] = None
    elif data['migration_in_testing_age'] is None:
        data['migration_result'] = 'never-in-testing'
    elif data['migration_in_testing_age'] > 1:
        data['migration_result'] = 'not-in-testing'
    elif data['migration_sync_age'] is None:
        # Should not happen as package is currently in testing. So, at some
        # point, testing and unstable versions should have been the same.
        data['migration_result'] = None
    else:
        data['migration_result'] = 'trying-to-migrate'

    if data['excuses_dependencies']:
        data['excuses_dependencies'] = json.loads(data['excuses_dependencies'])

    if data['migration_result'] or (row['migration_policy_verdict'] not in (
            None, 'PASS', 'REJECTED_TEMPORARILY')):
        for task in row['tasks']:
            qa_data[task]['migrations'] += 1

    return data


def _get_piuparts_data_from_row(qa_data, row):
    """Return piuparts data from a single database row."""
    if row['piuparts_fail']:
        for task in row['tasks']:
            qa_data[task]['piuparts'] += 1

    source_directory = _get_source_directory(row['source'])
    return {
        'piuparts_fail':
        row['piuparts_fail'],  # array
        'piuparts_version':
        row['piuparts_version'],
        'piuparts_url': ('https://piuparts.debian.org/%section%/source/'
                         f'{source_directory}/{row["source"]}.html'),
    }


def _get_qa_data(blend_name, qa_data):
    """Fetch bugs of all dependencies and store them in a dictionary."""
    _execute_udd_query("EXECUTE query_qa_packages('%s')" % blend_name)
    if curs.rowcount <= 0:
        sys.stderr.write(
            "No information about QA features received for Blend %s\n" %
            blend_name)
        exit(1)

    for row in RowDictionaries(curs):
        for task in row['tasks']:
            qa_data[task]['total'] += 1

        if not row['score']:
            for task in row['tasks']:
                qa_data[task]['no_issues'] += 1

            continue

        data = {}
        data['source'] = row['source']
        data['source_directory'] = _get_source_directory(row['source'])
        data['ci'] = _get_ci_data_from_row(qa_data, row)
        data['ar'] = _get_ar_data_from_row(qa_data, row)
        data['ex'] = _get_excuses_data_from_row(qa_data, row)
        data['piu'] = _get_piuparts_data_from_row(qa_data, row)

        for task in row['tasks']:
            qa_data[task]['data'].append(data)


def _print_qa_summary(data):
    """Print a summary of QA data to console."""
    for task in data:
        print(task + ": autoremovals=%(autoremovals)s, "
              "migrations=%(migrations)s, autopkgtests=%(autopkgtests)s, "
              "piuparts=%(piuparts)s no_issues=%(no_issues)s/%(total)s" %
              (data[task]))


def _write_json_output(blend_name, data):
    """Output template data into a JSON file for debugging."""
    if not debug:
        return

    states = ['depends', 'suggests', 'done']
    file_name = blend_name + '_qa.json'
    with open(file_name, 'w') as file_handle:
        if debug > 1:
            for task in data:
                file_handle.write("*** %s ***\n" % task)
                for status in states:
                    if status in data[task]:
                        file_handle.write("%s\n" % status)
                        file_handle.write(json.dumps(data[task][status]))
                        file_handle.write("\n\n")

        file_handle.write(json.dumps(data))

    SetFilePermissions(file_name)


def _get_template_context(blend_name, qa_data, config):
    """Return context variables for generating a template."""
    # Initialize i18n
    _ = gettext.translation(domain='blends-webtools', fallback=True).gettext

    data = {}
    data['_'] = _
    data['projectname'] = blend_name
    data['qa_data'] = qa_data
    if config.get('advertising') is not None:
        # we have to remove the gettext _() call which was inserted into the
        # config file to enable easy input for config file editors - but the
        # call has to be made explicitely in the python code
        advertising = re.sub(r'_\(\W(.+)\W\)', '\\1', config['advertising'])
        # gettext needs to escape '"' thus we need to remove the escape
        # character '\'
        data['projectadvertising'] = Markup(re.sub('\\\\"', '"', advertising))
    else:
        data['projectadvertising'] = None

    data['lang'] = 'en'
    data['updatetimestamp'] = formatdate(
        time.mktime(datetime.now().timetuple()))
    data['migration_verdicts'] = {
        'REJECTED_CANNOT_DETERMINE_IF_PERMANENT':
        _('Maybe temporary, maybe blocked but Britney is missing information'),
        'REJECTED_PERMANENTLY':
        _(' Rejected/violates migration policy/introduces a regression'),
        'REJECTED_TEMPORARILY':
        _('Waiting for test results, another package or too young '
          '(no action required now - check later)'),
        'REJECTED_NEEDS_APPROVAL':
        _('Needs an approval (either due to a freeze, '
          'the source suite or a manual hint)'),
        'REJECTED_BLOCKED_BY_ANOTHER_ITEM':
        _('Cannot migrate due to another item, which is blocked '
          '(please check which dependencies are stuck)'),
        'REJECTED_WAITING_FOR_ANOTHER_ITEM':
        _('Waiting for another item to be ready to migrate '
          '(no action required now - check later)'),
        'PASS_HINTED':
        _('The migration item did not pass the policy, but the failure is believed to be temporary '
          '(no action required now)'),
        'PASS':
        _('No excuse'),
    }

    for key in ('css', 'homepage', 'projecturl', 'projectname', 'logourl',
                'ubuntuhome', 'projectubuntu'):
        data[key] = config[key]

    return data


def main():
    """Fetch QA data from database and generate HTML files."""
    if len(sys.argv) <= 1:
        sys.stderr.write("Usage: %s <Blend name>\n" % sys.argv[0])
        sys.exit(-1)

    blendname = sys.argv[1]
    config = ReadConfig(blendname)

    # Prepare queries
    for query in queries:
        _execute_udd_query(query)

    # Fetch data from database
    qa_data = _get_tasks(blendname)
    _get_qa_data(blendname, qa_data)

    # Debug output
    _print_qa_summary(qa_data)
    _write_json_output(blendname, qa_data)

    # Generate HTML files from templates
    template_dir = os.path.join(os.path.dirname(__file__), 'templates')
    loader = TemplateLoader([template_dir],
                            auto_reload=True,
                            default_encoding="utf-8")

    # FIXME: as long as we are not finished use different dir
    outputdir = CheckOrCreateOutputDir(config['outputdir'], 'qa')
    if outputdir is None:
        sys.exit(-1)

    data = _get_template_context(blendname, qa_data, config)
    for task in qa_data:
        data['task'] = task

        template = loader.load('qa.xhtml')
        with open(outputdir + '/' + task + '.html', 'w',
                  encoding='utf-8') as file_handle:
            rendered_output = template.generate(**data).render('xhtml')
            file_handle.write(rendered_output)
        SetFilePermissions(outputdir + '/' + task + '.html')

    template = loader.load('qa_idx.xhtml')
    outputfile = outputdir + '/index.html'
    with open(outputfile, 'w', encoding='utf-8') as file_handle:
        rendered_output = template.generate(**data).render('xhtml')
        file_handle.write(rendered_output)
    SetFilePermissions(outputfile)


if __name__ == '__main__':
    main()
