#!/usr/bin/python3
#
# Generate web page listing screen shots of all packages with a given tag.
# Migrated from http://debian-edu.alioth.debian.org/cgi-bin/edu-debtags.cgi

import psycopg2
from re import split, sub
import optparse

parser = optparse.OptionParser(version='0.0')
(options, args) = parser.parse_args()

keytag = args[0] or "use::gameplaying"
grouptag = args[1] or "game"

query = """
SELECT package, tag from debtags
  WHERE tag like '%s::%%'
    AND package IN (SELECT package FROM debtags
                      WHERE tag = '%s'
                        AND package IN (SELECT package FROM debtags
                                         WHERE tag = 'role::program' and
                                               package in (select package from debtags
                                                           where tag = 'interface::x11')))
  ORDER BY tag, package;
""" % (grouptag, keytag)

# PORT = 5441
UDDPORT = 5452
PORT = UDDPORT
DEFAULTPORT = 5432

###########################################################################################
# Define several prepared statements to query UDD
connection_pars = [
    {'service': 'udd'},
    {'host': 'localhost', 'port': PORT, 'user': 'guest', 'database': 'udd'},
    {'host': 'localhost', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Hmmm, I observed a really strange behaviour on one of my machines where
    # connecting to localhost does not work but 127.0.0.1 works fine.  No idea
    # why ... but this should do the trick for the moment
    {'host': '127.0.0.1', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Public UDD mirror as last resort
    {'host': 'udd-mirror.debian.net', 'port': 5432,
     'user': 'udd-mirror', 'password': 'udd-mirror', 'database': 'udd'},
]
conn = None
for par in connection_pars:
    try:
        conn = psycopg2.connect(**par)
        break
    except psycopg2.OperationalError as err:
        continue

if conn is None:
    raise Exception('Unable to create connection to UDD')

conn.set_client_encoding('utf-8')
cur = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')

cur.execute(query)
rows = cur.fetchall()
cur.close()
conn.close()

print("""
<html>
<head>
<title>%s debtagged software</title>
</head>
<body>
<h1>%s debtagged software</h1>

<p>Packages tagged %s, role::program and interface::x11.
Please <a href="https://debtags.debian.net/">add debtags</a> if a program
are missing in the list, and <a href="https://screenshots.debian.net/">add
screenshots</a> if no-one did so already.</p>
""" % (keytag, keytag, keytag))

lastfieldtag = None
for row in rows:
    (package, fieldtag) = row
    if lastfieldtag != fieldtag:
        if lastfieldtag is not None:
            print("</p>")
        print("<strong>%s</strong>" % fieldtag)
        print("<p>")
        lastfieldtag = fieldtag
    print("<a href=\"https://packages.debian.org/search?searchon=names&exact=1&suite=all&section=all&keywords=%s\"><img src=\"https://screenshots.debian.net/thumbnail/%s\"></a>" % (package, package))
if lastfieldtag is not None:
    print("</p>")

print("""
</body>
</html>
""")
