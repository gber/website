#!/bin/sh
# This query is gathering data that would be needed to reactivate the service
#    "Weekly infolist of updatable packages for the debichem project"
#
# see example mail at
#    http://lists.alioth.debian.org/pipermail/debichem-devel/2011-March/003038.html

psql udd  <<EOT
SELECT distinct u.source, unstable_version, unstable_upstream 
  FROM blends_dependencies b
  JOIN packages p ON p.package = b.package
  JOIN dehs u     ON u.source = p.source
  WHERE blend = '$1' AND b.distribution = 'debian' AND unstable_status = 'outdated'
  ORDER BY source;
EOT

